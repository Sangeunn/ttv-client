import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { LandingRoutingModule } from './view/landing/landing.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { IndexRoutingModule } from './view/index/index.module';
import { ApiService } from '../api/api.service';
import { APP_BASE_HREF } from '@angular/common';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [AppComponent],
  imports: [
    LandingRoutingModule,
    IndexRoutingModule,
    BrowserModule,
    AppRoutingModule,
  ],
  providers: [{ provide: environment.baseUrl, useValue: '' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
