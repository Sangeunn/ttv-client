import { NgModule } from '@angular/core';
import { LandingComponent } from './landing/landing.component';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from '../app.component';
import { IndexComponent } from './index/index.component';

export const ROUTES: Routes = [
  { path: '', redirectTo: 'home' },
  { path: 'home', component: LandingComponent },
  { path: 'intro', component: IndexComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(ROUTES)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
