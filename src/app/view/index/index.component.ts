import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import * as $ from "jquery";
// import * as ScrollMagic from 'scrollmagic';

@Component({
  selector: "app-index",
  templateUrl: "./index.component.html",
  styleUrls: ["./index.component.css"],
})
export class IndexComponent implements OnInit {
  ngOnInit(): void {}
  ngAfterViewInit(): void {
    $(window).on("mousewheel", function (e: any) {
      var wheelDelta = e.originalEvent.wheelDelta;
      e.preventDefault();

      $(".section2").each(function () {
        var position_x = $(".section2").offset().left;

        if (position_x < 120) {
          console.log(position_x);
          $("#footer-11").animate({ opacity: "0" }, 500);
          $("#footer-12").animate({ opacity: "0.8" }, 500);
          $("#logo").css("display", "none");
          $("#logo2").animate({ opacity: "1" }, 500);
          //$("#logo").attr('src','./assets/images/12/section2/i-logo-ttv-head-w.png');
        }
      });

      if (wheelDelta > 0) {
        $(this).scrollLeft(-wheelDelta + $(this).scrollLeft());
      } else {
        $(this).scrollLeft(-wheelDelta + $(this).scrollLeft());
      }
      e.preventDefault();
    });
    $("#scrollbar").on("mousewheel", function (e: any) {
      var wheelDelta = e.originalEvent.wheelDelta;
      e.preventDefault();

      $("#text").each(function () {
        var position_x = $("#text").offset().left;
        var middle_position_x = $("#middle-text").offset().left;
        var bottom_position_x = $("#bottom-text").offset().left;
        var gif_position_x = $("#gif").offset().left;
        var section4_text = $("#section4-left-text").offset().left;
        var section4_right_text = $("#section4-right-top").offset().left;
        var section5_box1_left = $("#box1-left").offset().left;
        var section5_box1 = $("#box1").offset().left;
        var section5_box1_right = $("#box1-right").offset().left;
        var section5_desc = $("#description").offset().left;
        var section5_phone = $("#phone").offset().left;
        var section5_line = $("#line").offset().left;
        var section6_bodyright = $("#section6-body-right").offset().left;
        var section7_top = $("#top-body").offset().left;
        var section8_body = $("#section8-body").offset().left;
        var section11_right = $("#section11-right").offset().left;
        // var  = $('#').offset().left;
        // var  = $('#').offset().left;

        console.log("1 :", position_x);
        console.log("2 :", section5_phone);
        console.log("3 :", section5_line);
        if (position_x < 1000) {
          $("#text .title").animate({ opacity: "1" }, 300);
          $("#text .comment").animate({ opacity: "1" }, 400);
          $("#text .comment2").animate({ opacity: "1" }, 500);
          $("#bg-left")
            .delay(500)
            .animate({ bottom: "-29%", opacity: "1" }, 1000);
        }
        if (middle_position_x < 1500) {
          $("#middle-text").animate({ opacity: "1" }, 500);
        }
        if (bottom_position_x < 500) {
          $("#bottom-text").animate({ opacity: "1" }, 500);
          $("#bg-top").animate({ top: "35%" }, 1000);
        }
        if (gif_position_x < 1470) {
          $("#gif").animate({ opacity: "1" }, 500);
        }
        if (section4_text < 1700) {
          $("#section4-left-text").animate({ opacity: "1" }, 500);
        }
        if (section4_right_text < 1600) {
          $("#section4-right-top").animate({ opacity: "1" }, 500);
        }
        if (section5_box1_left < 1600) {
          $("#box1-left").animate({ opacity: "1" }, 500);
        }
        if (section5_box1 < 1000) {
          $("#box1").animate({ "padding-top": "10%" }, 500);
          $("#box1-right").animate({ opacity: 1 }, 500);
        }
        if (section5_desc < 1300) {
          $("#description").animate({ opacity: 1 }, 500);
        }
        if (section5_phone < 1200) {
          $("#phone").animate({ top: "83%" }, 500);
        }
        if (section5_line < 1200) {
          $("#line").animate({ opacity: 1 }, 500);
          $("#box2").animate({ opacity: 1 }, 500);
          $(".section5 .box2 .right .gif-with-bg img").animate(
            { bottom: 0 },
            500
          );
        }
        if (section6_bodyright < 1400) {
          $("#section6-body-right").animate({ opacity: 1 }, 500);
        }
        if (section7_top < 1500) {
          $("#top-body").animate({ opacity: 1, "padding-top": "3%" }, 500);
        }
        if (section8_body < 1500) {
          $("#section8-body").animate({ opacity: 1 }, 500);
        }
        if (section11_right < 1500) {
          $("#section11-right").animate({ opacity: 1, "padding-left": "5%" });
        }
      });

      if (wheelDelta > 0) {
        $(this).scrollLeft(-wheelDelta + $(this).scrollLeft());
      } else {
        $(this).scrollLeft(-wheelDelta + $(this).scrollLeft());
      }
      e.preventDefault();
    });
  }
}
