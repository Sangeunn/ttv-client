module.exports = {
  apps: [
    {
      script: "index.js",
      watch: ".",
    },
    {
      script: "./service-worker/",
      watch: ["./service-worker"],
    },
  ],

  deploy: {
    production: {
      user: "bitnami",
      host: "13.209.156.3",
      ref: "origin/master",
      repo: "git@github.com:sangeunnn/TTV-front.git",
      path: "",
      "pre-deploy-local": "",
      "post-deploy": "npm install",
      "pre-setup": "",
    },
  },
};
